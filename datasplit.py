from LAC import LAC
from sklearn.model_selection import train_test_split

filePath = 'taobao_onecol.txt'

fileTrainRead = []
with open(filePath, 'r') as fileTrainRaw:
    for line in fileTrainRaw:  # 按行读取文件
        fileTrainRead.append(line)

# 干预后结果
X_train, X_test = train_test_split(fileTrainRead, test_size=0.3, random_state=42)

ftrain = open("taobao_onecol_train.txt", "w",encoding='utf-8')

for rowtrain in X_train:
    ftrain.write(rowtrain)

ftrain.close()

ftest = open("taobao_onecol_test.txt", "w",encoding='utf-8')

for rowtest in X_test:
    ftest.write(rowtest)

ftest.close()