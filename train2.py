from LAC import LAC

# 选择使用默认的词法分析模型
lac = LAC()

# 训练和测试数据集，格式一致
train_file = "taobao_onecollabel_train.txt"
test_file = "taobao_onecollabel_test.txt"
lac.train(model_save_dir='./taobao_onecollabel_model',train_data=train_file, test_data=test_file)

# 使用自己训练好的模型
#my_lac = LAC(model_path='taobao_onecollabel_model')