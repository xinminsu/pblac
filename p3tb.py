from LAC import LAC
lac = LAC()

# 装载干预词典, sep参数表示词典文件采用的分隔符，为None时默认使用空格或制表符'\t'
lac.load_customization('taobao_onecollabel.txt', sep=None)

filePath = 'taobao_noblanksort.txt'

def wordDRL(word):
    if '\n' in word:
        word=word[:-1]
    return word

def removeRL(texts):
    texts = [wordDRL(xs) for xs in texts ]
    if u" "  in texts:
        texts.remove(u" ")
    if " "  in texts:
        texts.remove(" ")
    if '' in texts:
        texts.remove('')
    if '\n' in texts:
        texts.remove('\n')
    return texts

def processLine(line):
    result = []
    result.append(removeRL(line[0]))
    result.append(line[1])
    return result

fileTrainRead = []
with open(filePath, 'r') as fileTrainRaw:
    for line in fileTrainRaw:  # 按行读取文件
        fileTrainRead.append(line)

# 干预后结果
custom_result = [ processLine(x) for x in lac.run(fileTrainRead) ]

print(custom_result)